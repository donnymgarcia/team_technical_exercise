TEAM DevOps Engineer Technical Exercise
=========================================

Problem
-----------

+ Requirements:

1. AWS CloudFormation template that spins up the following resources in a given VPC of choice:

    * t3.large instance
    * security groups to allow Jira to operate
    * RDS instance to be used by Jira
    
2. Ansible playbook to be executed after the resources are provisioned on AWS to install the latest enterprise release of Jira server.


Solution
-----------

Solution has been included in Cfn template *team_technical_excercise.yaml*

+ Assumptions/Limitations:
    
    * VPC is selected by user.
    * RDS must be deployed to at least 2 availability zones in a VPC. This requirement is addressed by requiring user to set *DBAvailabilityZone1* and *DBAvailabilityZone2* in cfn parameters. For below solution we will require 2 x (different) AZs and 2 x subnets for RDS launch. 
    * *DBCidr1* and *DBCidr2* are 2 subnets required for RDS installation related to availability zone requirement mentioned above.
    * For *DBInstance*, *AllocatedStorage (100)*, *DBInstanceClass (db.t2.small)*, and *Engine (MySQL)* have been preconfigured at launch.
    * RDS MySQL version is mysql5.7.
    * For *DBSecurityGroup*, port 3306 is preconfigured for RDS instance.
    * For parameter *KeyName*, user must have a working keypair already configured to be applied to instance launch.
    * For Jira server *LatestAmiId* will launch *ami-amazon-linux-latest* *amzn2-ami-hvm-x86_64-gp2*. Because of this, yum is the package installer to be used by UserData block. This limits us to variants of Linux which support RPM based software packages. If using apt-get the UserData shell script must be updated to include logic for support for other Linux variants.
    * *Jira Core* *atlassian-jira-core* will be used for the Jira install. We will skip installations for *Jira Software* and *Jira Service Desk*.
    * From the [Jira Installation Manual](https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-938846841.html) below solution will address installation up to completing running of the Jira installer.  Automating the database configuration step will not be included in the scope of below solution. It is assumed that this will be done via the setup wizard in your browser. 
  
+ Details:

1.
```
## t3.large instance is created via EC2Instance resource.
  EC2Instance:
    Type: AWS::EC2::Instance
    Properties:
      InstanceType: !Ref 'InstanceType'
      SecurityGroupIds: [!GetAtt 'InstanceSecurityGroup.GroupId']
      KeyName: !Ref 'KeyName'
      ImageId: !Ref 'LatestAmiId'
      SubnetId: !Ref 'InstanceSubnet'
      UserData:
        Fn::Base64: 
        <snip>
```

```
## security groups to allow Jira to operate is created via InstanceSecurityGroup resource.
  InstanceSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Enable SSH access via 22 and HTTP access via 8080
      VpcId: !Ref 'VPC'
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 22
        ToPort: 22
        CidrIp: !Ref 'SSHCidr'
      - IpProtocol: tcp
        FromPort: 8080
        ToPort: 8080
        CidrIp: !Ref 'HTTPCidr' 
```

```
## RDS instance to be used by Jira created via DBInstance resource.
  DBInstance:
    Type: AWS::RDS::DBInstance
    Properties:
      DBName: !Ref 'DBName'
      AllocatedStorage: '100'
      DBInstanceClass: db.t2.small
      Engine: MySQL
      MasterUsername: !Ref 'DBUser'
      MasterUserPassword: !Ref 'DBPassword'
      DBSubnetGroupName: !Ref 'DBSubnetGroup'
      VPCSecurityGroups: [!GetAtt DBSecurityGroup.GroupId]
      DBParameterGroupName: !Ref 'DBParameterGroup'
```

2.
Ansible playbook has been included in UserData block of EC2Instance to be run run only during the boot cycle when you first launch an instance.
```
      UserData:
        Fn::Base64: 
          !Sub |
            #!/bin/bash -xe
            yum update -y
            amazon-linux-extras install ansible2 -y
            cat <<'EOF' >>main.yml
            ---
            - name: Install jira
              hosts: localhost
              connection: local
              become: yes

              tasks:
              - name: Install fontconfig
                package:
                  name: fontconfig
                  state: present

              - name: Download jira-core
                get_url:
                  url: https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-8.7.0-RC01-x64.bin
                  dest: /tmp/
                  mode: 'a+x'

              - name: Create .install4j directory
                file:
                  path: /tmp/.install4j
                  state: directory

              - name: Install response.varfile for unattended installation
                blockinfile:
                  create: yes
                  path: /tmp/.install4j/response.varfile
                  block: |
                    # install4j response file for Jira Core 8.7.0-m0005
                    app.install.service$Boolean=true
                    app.jiraHome=/var/atlassian/application-data/jira
                    existingInstallationDir=/opt/Jira Core
                    launch.application$Boolean=false
                    portChoice=default
                    sys.adminRights$Boolean=true
                    sys.confirmedUpdateInstallationString=false
                    sys.installationDir=/opt/atlassian/jira
                    sys.languageId=en

              - name: Run jira-core installer
                command: /tmp/atlassian-jira-core-8.7.0-RC01-x64.bin -q -varfile /tmp/.install4j/response.varfile

              - name: Unarchive MySQL JDBC driver tar.gz
                unarchive:
                  src: https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.48.tar.gz
                  dest: /tmp/
                  remote_src: yes

              - name: Copy MySQL JDBC driver
                copy:
                  src: /tmp/mysql-connector-java-5.1.48/mysql-connector-java-5.1.48.jar
                  dest: /opt/atlassian/jira/lib/

              - name: Copy MySQL JDBC driver bin
                copy:
                  src: /tmp/mysql-connector-java-5.1.48/mysql-connector-java-5.1.48-bin.jar
                  dest: /opt/atlassian/jira/lib/

              - name: Start jira service
                command: /etc/init.d/jira start
            EOF
            ansible-playbook main.yml
```

+ Parameters:

Below are example parameter values to use on stack creation.

![Parameter Example](/images/CfnParams.png)

1. For above, *DBCidr1*, *DBCidr2*, and *InstanceCidr* have been configured to default values for below subnets:
   
      * DBCidr1: 172.30.3.0/24
      * DBCidr2: 172.30.4.0/24
      * InstanceCidr: 172.30.5.0/24
    
2. Feel free to adjust subnet values based on your current VPC/region set up. Subnets provided should not conflict with existing subnets already in use or to be created.

3. For above, *HTTPCidr*, *SSHCidr*, and *DBCidr* are subnets to be used by security groups. Adjust based on your own security practices. Parameter default has been configured to *0.0.0.0/0*.

4. When creating a new Jira stack take note of values used for *DBName*, *DBPassword*, and *DBUser*. These values will be needed to connect to your database during the Jira setup wizard.

+ Outputs:

![CREATE_COMPLETE Outputs](/images/JIRAStack_CREATE_COMPLETE.png)

1. After a successful `Create stack` check the *Outputs* tab in the Cloudformation section in the AWS console. There you will find values about the stack to complete install via the setup wizard and values needed to set up the database. For example, RDS hostname provided by *JDBCConnectionString* and *JiraURL*.


2. After clicking the Jira URL link provided you should arrive at the Jira setup wizard screen. From here you can complete the remaining steps to install Jira.

    * Set application properties
    * Enter your license
    * Create your administrator account
    * Set up email notifications
    * Start using Jira
  
+ Bonus points:

1. Ansible playbook that runs automatically after the AWS CloudFormation template has provisioned the resources.
  
    * Added to UserData section of *EC2Instance* resource. (*See: Line 232 ansible-playbook main.yml*)

2. Ansible playbook able to update a custom DNS Domain and Jira should be able to use the custom hostname instead of the AWS provided external domain name.
    
    * Included *update_dns_domain.yaml* as a rough POC. Not enough testing had been completed, but solution would be along the lines of this playbook (to give you an idea).

3. Good documentation via README.md

    * You're reading the README.md now. Thanks for reading! :)